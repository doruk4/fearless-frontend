window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    const responseConferences = await fetch(url);  // Changed variable name

    if (responseConferences.ok) {
        const data = await responseConferences.json();
        const selectTag = document.getElementById('conference')
        for (let conference of data.conferences){
            const newConference = document.createElement('option')
            newConference.value = conference.id
            newConference.innerHTML = conference.name
            selectTag.appendChild(newConference)
        }
    }

    const formTag = document.getElementById('create-presentation-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const selectTag = document.getElementById('conference')
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        const conferenceID = selectTag.selectedIndex
        const conferenceUrl = `http://localhost:8000/api/conferences/${conferenceID}/presentations/`;
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const responsePresentation = await fetch(conferenceUrl, fetchConfig);

        if (responsePresentation.ok) {
            formTag.reset();
            const newPresentation = await responsePresentation.json();
            console.log(newPresentation);
        }
    });
});
