window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/locations/';
    const responseLocations = await fetch(url);  // Changed variable name

    if (responseLocations.ok) {
        const data = await responseLocations.json();
        const selectTag = document.getElementById('location')
        for (let location of data.locations){
            const newLocation = document.createElement('option')
            newLocation.value = location.id
            newLocation.innerHTML = location.name
            selectTag.appendChild(newLocation)
        }
    }

    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const responseConferences = await fetch(conferenceUrl, fetchConfig);

        if (responseConferences.ok) {
            formTag.reset();
            const newConference = await responseConferences.json();
            console.log(newConference);
        }
    });
});
